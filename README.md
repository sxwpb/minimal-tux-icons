# minimal tux icons by sxwpb

![preview of the icons](images/preview1.jpg)
icons shaped/designed by sxwpb

## downloads

[SVG.zip](/uploads/d3a9abffed046285954db402f4fe6c18/SVG.zip)
[PNG.zip](/uploads/7506e61b5962e2ba5eb1fa51a52b31f9/PNG.zip)
[DXF.zip](/uploads/6ebd20713f694b92786f18d41b1d0849/DXF.zip)

## example uses

![possible usecase 1](images/bitmap.png)

keychain idea (rendered with blender)
![rendered example](images/render.png)

## other

the icon was entirely shaped in SolveSpace for the fks of it.

![screenshot of Solvespace constraints](images/screenshot-solvespace.png)

## leave a donation ```¯\(°_o)/¯```

a donation is the BEST way to help me out.

https://donate.stripe.com/fZe8zh2wSgH17IIcMM

## icons license

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1)
